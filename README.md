# Installing isttomo

### Dependencies

isttomo relies on SDAS to retrieve data from ISTTOK's database.
Install SDAS following [these instructions](http://metis.ipfn.ist.utl.pt/CODAC/IPFN_Software/SDAS/Access/Python).

### Installing

We recommend that you install isttomo using pip. Download the [isttomo-master.tar.gz](https://gitlab.com/Europium-152/isttomo/-/archive/master/isttomo-master.tar.gz)
file, and run:
```bash
pip install isttomo-master.tar.gz
```
If you don not have pip, extract the contents of [isttomo-master.tar.gz](https://gitlab.com/Europium-152/isttomo/-/archive/master/isttomo-master.tar.gz)
and run:
```bash
python setup.py install
```
To uninstall isttomo, simply run:
```bash
pip uninstall isttomo
```

# Using isttomo

With isttomo you can get the reconstruction of a given time instant during a plasma discharge at ISTTOK. Example usage:
```python
from isttomo import tomogram
import matplotlib.pyplot as plt

# Obtaining the Reconstruction -----------------

r = tomogram(shot=47220,  # Number of the shot
             reconstruction_times=30000,  # Time instant in microseconds
             target_chi2=0.03,  # Chi-squared target value
             alpha=1e-7)  # Initial guess for the regularization strength

# Plotting the Result --------------------------

plt.pcolormesh(r.x_axis, r.y_axis, r.emissivity)
plt.show()
```

The tomogram function returns a tomogram object with the following properties:

* ```x_axis```, ```y_axis``` - x and y coordinates of the corners of pixels in the tomogram.

* ```emissivity``` - reconstructed emissivity profile, stored in a matrix.

* ```time``` - time instant used for the tomogram. 
This can differ from the one specified because data at ISTTOK is stored at a specific sample rate.

* ```signals``` - signals measured by the cameras.

* ```retrofit``` - retrofitted signals. Signals that would be measured by the cameras 
if the plasma where to be exactly like the reconstructed.