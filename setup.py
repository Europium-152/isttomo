import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="isttomo",
    version="0.0.1",
    author="Daniel Hachmeister",
    author_email="daniel.hachmeister@tecnico.ulisboa.pt",
    description="Tomography toolkit for tokamak ISTTOK",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Europium-152/isttomo",
    packages=setuptools.find_packages(),
    package_data={'': ['*.npy'], 'isttomo': ['cache/delete.npy']},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)

"""
To install in development mode run 'pip install -e .'
source: https://stackoverflow.com/questions/2087148/can-i-use-pip-instead-of-easy-install-for-python-setup-py-install-dependen
"""
