import os
import numpy as np

# TODO: Read all possible projection matrix files from dir projection-matrices
valid_projections = [
    "complex-view-cone-45.npy",
]

# Module directory. Use to fetch the projection files
this_directory = os.path.dirname(__file__) + '\\'


def load_projection(projection_file_name):

    try:
        return np.load(this_directory + projection_file_name)
    except IOError:
        raise IOError("'%s' is not a valid projection. Select one from:\n" % (this_directory + projection_file_name)
                      + str(valid_projections))