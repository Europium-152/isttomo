from .tomography import MFI
from .prepare_signals import prepare_plasma_signals
import numpy as np
import colorama as cl


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx, array[idx]


class Tomogram:

    def __init__(self, x_axis, y_axis, emissivity, time, signals, retrofit):

        self._x_axis = x_axis
        self._y_axis = y_axis
        self._emissivity = emissivity
        self._time = time
        self._signals = signals
        self._retrofit = retrofit

    @property
    def x_axis(self):
        return np.array(self._x_axis)

    @property
    def y_axis(self):
        return np.array(self._y_axis)

    @property
    def emissivity(self):
        return np.array(self._emissivity)

    @property
    def time(self):
        return np.array(self._time)

    @property
    def signals(self):
        return np.array(self._signals)

    @property
    def retrofit(self):
        return np.array(self._retrofit)


def tomogram(shot, reconstruction_times, target_chi2, alpha, verbose=False):
    """ Perform the MFI reconstruction at a given time instant for a certain shot

    Parameters
    ----------
    shot: int
        Shot number e.g. 45765
    reconstruction_times: float
        Time in us (micro-seconds)
    target_chi2: float
        Chi-squared target value for the reconstruction algorithm.
    alpha: float
        Initial guess for the regularization weight alpha.
    verbose: bool, optional
        Print accessory information useful for debugging. Defaults to False

    Returns
    -------
    times: float, ndarray
        Time instant(s) where the reconstruction was performed.
    signals: float, ndarray
        Preprocessed signals from the tomography cameras.
    reconstructions: tomography.OuterLoopOutput class instance
        Reconstruction(s) performed at the given time instant(s)
    mfi: tomography.MFI class instance
        MFI class with useful attributes for data analysis and algorithm information

    """

    # Load and preprocess tomography signals ------------------------------------------------------

    signal_times, data, projections_dic = prepare_plasma_signals(shot=shot, verbose=verbose)

    projections = projections_dic['projections']
    print("Projections:", projections.size) if verbose else None

    # Figure what time instants to use for the reconstruction ------------------------------------

    if hasattr(reconstruction_times, '__iter__'):
        pass
    else:
        reconstruction_times = [reconstruction_times]

    tomo_signals = []
    times = []
    for reconstruction_time in reconstruction_times:
        print(("Perform reconstruction for time instant %f" % reconstruction_time)) if verbose else None
        time_index, time = find_nearest(signal_times, reconstruction_time)
        print(("Associated time index is %d, actual stored time is %f" % (time_index, time))) if verbose else None
        tomo_signals.append(data[time_index, :32])
        times.append(time)

    # Instantiate MFI class ----------------------------------------------------------------------
    mfi = MFI(projections, width=200., height=200., mask_radius=85.)

    print("Performing %d reconstructions" % len(tomo_signals))

    reconstructions = []
    divergence_counter = 0
    for t, f in zip(times, tomo_signals):

        # Normalization of signals (cheating) ---------------------------------------------------------
        total_plasma_top = np.sum(f[:16] * np.sum(projections[:16], axis=(-1, -2)) ** (-1))
        total_plasma_out = np.sum(f[16:32] * np.sum(projections[16:32], axis=(-1, -2)) ** (-1))

        print("Total plasma ratio out/top", total_plasma_out / total_plasma_top) if verbose else None

        f[16:32] = f[16:32] / (total_plasma_out / total_plasma_top)

        r = mfi.tomogram(signals=f,
                         stop_criteria=0.01,
                         alpha_3=1,
                         alpha_4=0,
                         target=target_chi2,
                         tolerance=0.005,
                         guess=alpha,
                         inner_max_iter=10,
                         outer_max_iter=10,
                         verbose=True)

        if not r.converged:
            divergence_counter += 1

        tomo = Tomogram(x_axis=np.array(mfi.x_array_plot), y_axis=np.array(mfi.y_array_plot),
                        emissivity=np.array(r.asmatrix),
                        time=t, signals=np.array(f), retrofit=np.array(mfi.retrofit(r.asarray)))

        reconstructions.append(tomo)

    if divergence_counter == 0:
        print(cl.Fore.GREEN + cl.Style.BRIGHT + "Minimum fisher converged for every reconstruction")
    else:
        print(cl.Fore.RED + "Convergence warning: Minimum fisher did not converge for %d out of %d reconstructions" %
              (divergence_counter, len(reconstructions)))

    # ---------------------------------------------------------------------------------

    if len(reconstructions) == 1:
        return reconstructions[0]
    else:
        return reconstructions
