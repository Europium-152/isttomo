from isttomo.projection_matrices.projection_selector import load_projection
import numpy as np


def get_tomography_setup(shot):
    if 47098 > shot >= 44880:  # Tomography p/ #44880 onward (18 dec 2018)
        channels = [
            'MARTE_NODE_IVO3.DataCollection.Channel_182',  # top_04
            'MARTE_NODE_IVO3.DataCollection.Channel_181',  # top_05
            'MARTE_NODE_IVO3.DataCollection.Channel_184',  # top_06
            'MARTE_NODE_IVO3.DataCollection.Channel_179',  # top_07
            'MARTE_NODE_IVO3.DataCollection.Channel_178',  # top_08
            'MARTE_NODE_IVO3.DataCollection.Channel_185',  # top_09
            'MARTE_NODE_IVO3.DataCollection.Channel_183',  # top_10
            'MARTE_NODE_IVO3.DataCollection.Channel_180',  # top_11
            'MARTE_NODE_IVO3.DataCollection.Channel_004',  # top_12
            'MARTE_NODE_IVO3.DataCollection.Channel_005',  # top_13
            'MARTE_NODE_IVO3.DataCollection.Channel_003',  # top_14
            'MARTE_NODE_IVO3.DataCollection.Channel_007',  # top_15
            'MARTE_NODE_IVO3.DataCollection.Channel_000',  # top_16
            'MARTE_NODE_IVO3.DataCollection.Channel_001',  # top_17
            'MARTE_NODE_IVO3.DataCollection.Channel_002',  # top_18
            'MARTE_NODE_IVO3.DataCollection.Channel_006',  # top_19

            'MARTE_NODE_IVO3.DataCollection.Channel_190',  # out_04
            'MARTE_NODE_IVO3.DataCollection.Channel_189',  # out_05
            'MARTE_NODE_IVO3.DataCollection.Channel_192',  # out_06
            'MARTE_NODE_IVO3.DataCollection.Channel_187',  # out_07
            'MARTE_NODE_IVO3.DataCollection.Channel_186',  # out_08
            'MARTE_NODE_IVO3.DataCollection.Channel_193',  # out_09
            'MARTE_NODE_IVO3.DataCollection.Channel_191',  # out_10
            'MARTE_NODE_IVO3.DataCollection.Channel_188',  # out_11
            'MARTE_NODE_IVO3.DataCollection.Channel_012',  # out_12
            'MARTE_NODE_IVO3.DataCollection.Channel_013',  # out_13
            'MARTE_NODE_IVO3.DataCollection.Channel_011',  # out_14
            'MARTE_NODE_IVO3.DataCollection.Channel_015',  # out_15
            'MARTE_NODE_IVO3.DataCollection.Channel_008',  # out_16
            'MARTE_NODE_IVO3.DataCollection.Channel_009',  # out_17
            'MARTE_NODE_IVO3.DataCollection.Channel_010',  # out_18
            'MARTE_NODE_IVO3.DataCollection.Channel_014',  # out_19

            'MARTE_NODE_IVO3.DataCollection.Channel_198',  # bot_04
            'MARTE_NODE_IVO3.DataCollection.Channel_197',  # bot_05
            'MARTE_NODE_IVO3.DataCollection.Channel_200',  # bot_06
            'MARTE_NODE_IVO3.DataCollection.Channel_195',  # bot_07
            'MARTE_NODE_IVO3.DataCollection.Channel_194',  # bot_08
            'MARTE_NODE_IVO3.DataCollection.Channel_201',  # bot_09
            'MARTE_NODE_IVO3.DataCollection.Channel_199',  # bot_10
            'MARTE_NODE_IVO3.DataCollection.Channel_196',  # bot_11
            'MARTE_NODE_IVO3.DataCollection.Channel_020',  # bot_12
            'MARTE_NODE_IVO3.DataCollection.Channel_021',  # bot_13
            'MARTE_NODE_IVO3.DataCollection.Channel_019',  # bot_14
            'MARTE_NODE_IVO3.DataCollection.Channel_023',  # bot_15
            'MARTE_NODE_IVO3.DataCollection.Channel_016',  # bot_16
            'MARTE_NODE_IVO3.DataCollection.Channel_017',  # bot_17
            'MARTE_NODE_IVO3.DataCollection.Channel_018',  # bot_18
            'MARTE_NODE_IVO3.DataCollection.Channel_022',  # bot_19
        ]
        projections_dic = load_projection("complex-view-cone-45-old-pinhole.npy")[0]

    elif 47167 <= shot <= 47301:  # Tomography calibrated without bottom camera
        channels = [
            'MARTE_NODE_IVO3.DataCollection.Channel_182',  # top_04
            'MARTE_NODE_IVO3.DataCollection.Channel_181',  # top_05
            'MARTE_NODE_IVO3.DataCollection.Channel_184',  # top_06
            'MARTE_NODE_IVO3.DataCollection.Channel_179',  # top_07
            'MARTE_NODE_IVO3.DataCollection.Channel_178',  # top_08
            'MARTE_NODE_IVO3.DataCollection.Channel_185',  # top_09
            'MARTE_NODE_IVO3.DataCollection.Channel_183',  # top_10
            'MARTE_NODE_IVO3.DataCollection.Channel_180',  # top_11
            'MARTE_NODE_IVO3.DataCollection.Channel_004',  # top_12
            'MARTE_NODE_IVO3.DataCollection.Channel_005',  # top_13
            'MARTE_NODE_IVO3.DataCollection.Channel_003',  # top_14
            'MARTE_NODE_IVO3.DataCollection.Channel_007',  # top_15
            'MARTE_NODE_IVO3.DataCollection.Channel_000',  # top_16
            'MARTE_NODE_IVO3.DataCollection.Channel_001',  # top_17
            'MARTE_NODE_IVO3.DataCollection.Channel_002',  # top_18
            'MARTE_NODE_IVO3.DataCollection.Channel_006',  # top_19

            'MARTE_NODE_IVO3.DataCollection.Channel_190',  # out_04
            'MARTE_NODE_IVO3.DataCollection.Channel_189',  # out_05
            'MARTE_NODE_IVO3.DataCollection.Channel_192',  # out_06
            'MARTE_NODE_IVO3.DataCollection.Channel_187',  # out_07
            'MARTE_NODE_IVO3.DataCollection.Channel_186',  # out_08
            'MARTE_NODE_IVO3.DataCollection.Channel_193',  # out_09
            'MARTE_NODE_IVO3.DataCollection.Channel_191',  # out_10
            'MARTE_NODE_IVO3.DataCollection.Channel_188',  # out_11
            'MARTE_NODE_IVO3.DataCollection.Channel_012',  # out_12
            'MARTE_NODE_IVO3.DataCollection.Channel_013',  # out_13
            'MARTE_NODE_IVO3.DataCollection.Channel_011',  # out_14
            'MARTE_NODE_IVO3.DataCollection.Channel_015',  # out_15
            'MARTE_NODE_IVO3.DataCollection.Channel_008',  # out_16
            'MARTE_NODE_IVO3.DataCollection.Channel_009',  # out_17
            'MARTE_NODE_IVO3.DataCollection.Channel_010',  # out_18
            'MARTE_NODE_IVO3.DataCollection.Channel_014',  # out_19
        ]
        calibration = [0.56227586, 0.63544848, 0.67819011, 0.69488891,
                       0.83896502, 0.86817544, 0.91787691, 1.00000000,
                       1.01697758, 1.00053751, 0.94677499, 0.91703750,
                       0.89061370, 0.74562260, 0.76703700, 0.97939542,

                       0.09582662, 0.22800629, 0.37249746, 0.48471883,
                       0.60597115, 0.72577020, 0.80735010, 0.87208898,
                       0.88737810, 0.86586066, 0.81099261, 0.71626144,
                       0.59320874, 0.49118714, 0.40307170, 0.40302172]
        projections_dic = load_projection("complex-view-cone-45-new-pinhole.npy")[0]
        projections_dic['projections'] = np.array(
            [p*c for p, c in zip(projections_dic['projections'][:32], calibration)])

    else:
        raise ValueError("No tomography data is available for shot #%d" % shot)

    return channels, projections_dic
