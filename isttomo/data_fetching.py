from sdas.tests.LoadSdasData import LoadSdasData
from sdas.tests.StartSdas import StartSdas
import numpy as np
import os

_package_directory = os.path.dirname(os.path.abspath(__file__))


def online_fetch(shot, uid, client='auto'):
    """
    Fetch data from ISTTOK database

    Parameters
    ----------
        shot: int
            Shot number
        uid: str
            Channel unique identifier
        client: (optional) Sdas client object
            Instance of Sdas Client. If not provided, a new client will be initiated to fetch the data.

    Returns
    -------
        x: numpy array
            Signal values
        t: numpy array
            Signal time coordinates
    """

    if client == 'auto':
        client = StartSdas()

    if shot == 0:
        shot = client.searchMaxEventNumber('0x0000')
    iplasma_ok = 0
    print('Downloading channel ' + str(uid) + ', SHOT #' + str(shot))
    while True:
        if iplasma_ok == 0:
            try:
                iplasma, iplasma_times = LoadSdasData(client, uid, shot);
                iplasma_ok = np.all(np.isfinite(iplasma))
            except:
                iplasma_ok = 0
        if iplasma_ok:
            break

    return np.array(iplasma), np.array(iplasma_times)


def local_fetch(shot, uid, cache=_package_directory + '/cache/'):
    """
    Retrieve data for a given shot and channel. Try to retrieve locally from the cache.
    If shot is not cached, then fetch from isttok database and save to cache.

    Parameters
    ----------
        shot: int
            Shot number
        uid: str
            Channel unique identifier
        cache: dir, optional
            Cache directory. Optional. Defaults to '<package-dir>/cache'

    Returns
    -------
        x: numpy array
            Signal values
        t: numpy array
            Signal time coordinates
    """

    # Find cached data
    try:
        data = np.load(cache + str(shot) + '_' + str(uid) + '.npy')
        print("Found: Requested data is cached. Loaded channel: " + str(uid) + ', SHOT #' + str(shot))
        return data[0], data[1]
    except IOError:
        print("Warning: Requested data not cached, fetching from ISTTOK")
        x, t = online_fetch(shot, uid)
        np.save(cache + str(shot) + '_' + str(uid), np.array([x, t]))
        return x, t



