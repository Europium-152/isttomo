import numpy as np
import matplotlib.pyplot as plt
from skimage.draw import ellipse
try:
    import cupy as cp
except ImportError:
    cp = None


plt.close("all")

plt.rcParams.update({'font.size': 18})


class OuterLoopOutput:
    """During the inner loop a tomogram is generated for a given regularization.
    The tomogram can be found iteratively (MFI) or directly (Linear Tikhonov).
    TODO: Investigate about overwriting getter methods for attributes

    Parameters
    ----------
    emissivity: ndarray
        Emissivity matrix shape(n_rows, n_cols)
    n_rows, n_cols: int
        Number of rows and columns on the reconstructed profile
    convergence_flag: bool
        If `True` it is assumed that the outerloop has converged in the sense that
        the successive iterations lead to progressively smaller changes in the emissivity profile.
    """

    def __init__(self, emissivity, alpha, n_rows, n_cols, convergence_flag=True):
        self._emissivity = emissivity
        self._alpha = alpha
        self._n_rows = n_rows
        self._n_cols = n_cols
        self._converged = convergence_flag

    @property
    def asarray(self):
        return np.array(self._emissivity).flatten()

    @property
    def asmatrix(self):
        return np.array(self._emissivity).reshape((self._n_rows, self._n_cols))

    @property
    def converged(self):
        return self._converged


class InnerLoopOutput:
    """During the inner loop a tomogram is generated for a given regularization.
    The tomogram can be found iteratively (MFI) or directly (Linear Tikhonov).
    TODO: Investigate about overwriting getter methods for attributes

    Parameters
    ----------
    iterations: ndarray
        list of the emissivity arrays ordered cronologically
    n_rows, n_cols: int
        Number of rows and columns on the reconstructed profile
    convergence_flag: boolean, optional
        If `True` it is assumed that the inner loop has converged in the sense that
        the successive iterations lead to progressively smaller changes in the emissivity profile.
    """

    def __init__(self, iterations, n_rows, n_cols, convergence_flag=True):
        self._iterations = iterations
        self._n_rows = n_rows
        self._n_cols = n_cols
        self._converged = convergence_flag

    def asarray(self):
        return np.array(self._iterations[-1])

    def asmatrix(self):
        return np.array(self._iterations[-1]).reshape((self._n_rows, self._n_cols))

    def converged(self):
        return self._converged


class MFI:

    def __init__(self, projections, width=200., height=200., mask_radius=85.,
                 enable_gpu=(True if cp is not None else False)):
        """
        TODO:
            Create the concept of a reconstruction object that is to be returned.
            This object should have attributes like resolution, coordinates, convergence flags, etc
        Parameters
        ----------
        projections: ND-array 32 x rows x columns
            Projections of each sensor, ordered coherently with "signals", see reconstruction().
            The number of rows and columns in the projection matrices are the number of rows and columns
            in the final reconstructed profile.
        width: float, optional
            With in millimeters of the reconstruction window. The resolution along xx axis will be
            width / number_of_columns
        height: float, optional
            Height in millimeters of the reconstruction window. The resolution along yy axis will be
            height / number_of_rows
        mask_radius: float, optional
            Reconstruction mask. The algorithm will impose a zero emissivity outside the specified radius
        """

        # Reconstruction Size and Resolution -----------------------------------------------

        self._projections = np.array(projections, dtype=np.float32)
        print('projections:', self._projections.shape, self._projections.dtype)

        n_rows = self._projections.shape[1]
        n_cols = self._projections.shape[2]
        res_x = width / float(n_cols)
        res_y = height / float(n_rows)  # x,y (mm)

        # x and y arrays for plotting purposes. Coordinates represent the top left corner of each pixel
        self._x_array_plot = (np.arange(n_cols + 1) - n_cols / 2.) * res_x
        self._y_array_plot = (n_rows / 2. - np.arange(n_rows + 1)) * res_y

        # x and y arrays for calculation purposes. Coordinates represent the center of each pixel
        self._x_array = np.arange(n_cols) * res_x - n_cols / 2. * res_x
        self._y_array = n_rows / 2. * res_y - np.arange(n_rows) * res_y

        # Masks, negative mask: zeros inside vessel, positive mask: zeros outside vessel -------------------------------

        ii, jj = ellipse(n_rows / 2., n_cols / 2., mask_radius / res_y, mask_radius / res_x)
        mask_negative = np.ones((n_rows, n_cols), dtype=np.float32)
        mask_negative[ii, jj] = 0.
        mask_positive = np.zeros((n_rows, n_cols), dtype=np.float32)
        mask_positive[ii, jj] = 1.

        # Apply mask to projection matrix and then reshape -------------------------------------------------------------

        P = (self._projections * mask_positive).reshape((self._projections.shape[0], -1))
        print('P:', P.shape, P.dtype)

        # x and y gradient matrices ----------------------------------------------

        Dh = np.eye(n_rows * n_cols, dtype=np.float32) - np.roll(np.eye(n_rows * n_cols, dtype=np.float32), 1, axis=1)
        Dv = np.eye(n_rows * n_cols, dtype=np.float32) - np.roll(np.eye(n_rows * n_cols, dtype=np.float32), n_cols, axis=1)

        # New matrices more correct (actually are a piece of shit) --------------------------------

        # Dh_element = np.diag(np.ones(n_cols - 1), k=1) - np.diag(np.ones(n_cols - 1), k=-1)
        # Dh_element[0, 0] = -2.
        # Dh_element[0, 1] = 2.
        # Dh_element[-1, -1] = 2.
        # Dh_element[-1, -2] = -2.
        #
        # Dh = 0.5*block_diag(*(Dh_element for i in range(n_rows)))
        #
        # Dv_element = np.append(np.diag(2. * np.ones(n_cols)), np.diag(-2. * np.ones(n_cols)), axis=1)
        # Dv_top = np.append(Dv_element, np.zeros((n_cols, n_cols * (n_rows - 2))), axis=1)
        #
        # Dv_mid = np.append(np.diag(np.ones(n_cols * (n_rows - 2))), np.zeros((n_cols * (n_rows - 2), n_cols * 2)),
        #                    axis=1) + \
        #          np.append(np.zeros((n_cols * (n_rows - 2), n_cols * 2)), np.diag(-1. * np.ones(n_cols * (n_rows - 2))),
        #                    axis=1)
        #
        # Dv_bot = np.roll(Dv_top, n_cols * (n_rows - 2), axis=1)
        #
        # Dv = np.append(Dv_top, Dv_mid, axis=0)
        # Dv = 0.5*np.append(Dv, Dv_bot, axis=0)
        #
        # print('Dh:', Dh.shape, Dh.dtype)
        # print('Dv:', Dv.shape, Dv.dtype)

        # Norm matrix --------------------------------------------------------------

        Io = np.eye(n_rows * n_cols, dtype=np.float32) * mask_negative.flatten()
        Ii = np.eye(n_rows * n_cols, dtype=np.float32) * mask_positive.flatten()
        # Io = np.eye(n_rows * n_cols)

        print('Io:', Io.shape, Io.dtype)

        # P transpose and PtP ------------------------------------------------------
        Pt = np.transpose(P)
        PtP = np.dot(Pt, P)

        # Norm matrix transposed ---------------------------------------------------
        ItIo = np.dot(np.transpose(Io), Io)
        ItIi = np.dot(np.transpose(Ii), Ii)

        # Aliasing -----------------------------------------------------------------
        self._Dh = np.array(Dh, dtype=np.float32)
        self._Dht = self._Dh.T
        self._Dv = np.array(Dv, dtype=np.float32)
        self._Dvt = self._Dv.T
        self._Pt = Pt
        self._PtP = PtP
        self._ItIo = ItIo
        self._ItIi = ItIi
        self._n_rows = n_rows
        self._n_cols = n_cols
        self._res_x = res_x
        self._res_y = res_y

        # GPU variable allocation --------------------------------------------------
        if enable_gpu:
            self._gpu_Dh = cp.array(Dh, dtype=cp.float32)
            self._gpu_Dht = cp.transpose(self._gpu_Dh)
            self._gpu_Dv = cp.array(Dv, dtype=cp.float32)
            self._gpu_Dvt = cp.transpose(self._gpu_Dv)
            self._gpu_Pt = cp.array(Pt, dtype=cp.float32)
            self._gpu_PtP = cp.array(PtP, dtype=cp.float32)
            self._gpu_ItIo = cp.array(ItIo, dtype=cp.float32)
            self._gpu_ItIi = cp.array(ItIi, dtype=cp.float32)
            self._gpu_enabled = enable_gpu

    @property
    def x_array_plot(self):
        return np.array(self._x_array_plot)

    @property
    def y_array_plot(self):
        return np.array(self._y_array_plot)

    @property
    def x_array(self):
        return np.array(self._x_array)

    @property
    def y_array(self):
        return np.array(self._y_array)

    @property
    def n_rows(self):
        return self._n_rows

    @property
    def n_cols(self):
        return self._n_cols

    @property
    def res_x(self):
        return self._res_x

    @property
    def res_y(self):
        return self._res_y

    def retrofit(self, g):
        """Find the retrofit signals of a given emissivity g"""

        return np.dot(self._Pt.T, np.array(g).flatten())

    # @profile
    def reconstruction(self, signals, stop_criteria, alpha_1, alpha_2, alpha_3, alpha_4, max_iterations,
                           iterations=False, guess=None, verbose=False):
        """Apply the minimum fisher reconstruction algorithm for a given set of measurements from tomography.

        input:
            signals: array
                Array of signals from each sensor ordered like in "projections".
            stop_criteria: float
                Average different between iterations to admit convergence as a percentage between 0 and 1.
            alpha_1, alpha_2, alpha_3, alpha_4: float
                Regularization weights. Horizontal derivative. Vertical derivative. Outside Norm. Inside Norm.
            max_iterations: int
                Maximum number of iterations before algorithm admits non-convergence
            iterations: boolean, optional
                If set to `True`, returns every iteration step as a reconstruction. Defaults to False.
            guess: ndarray, optional
                Initial guess for the reconstructed profile.
            verbose: boolean, optional
                Print inner convergence messages. Defaults to `False`

        output:
            inner_loop_output: InnerLoopOutput class instance
                Object holding the data from the mfi inner loop.
        """

        # Aliasing for cleaner code --------------------------------------------------
        # Dh = self._np.array(self._Dh, dtype=np.float32)
        # Dht = np.transpose(Dh)
        # Dv = np.array(self._Dv, dtype=np.float32)
        # Dvt = np.transpose(Dv)
        # Pt = np.array(self._Pt, dtype=np.float32)
        n_rows = self._n_rows
        n_cols = self._n_cols

        Dh = self._Dh
        Dht = self._Dht
        Dv = self._Dv
        Dvt = self._Dvt
        Pt = self._Pt
        PtP = self._PtP
        ItIi = self._ItIi
        ItIo = self._ItIo

        # Instantiate f vector of signals --------------
        f = np.array(signals, dtype=np.float32)

        # -----------------------------  FIRST ITERATION  -------------------------------------------------------------

        # First guess to g is uniform plasma distribution --------------------------
        if guess is None:
            g_old = np.ones(n_rows * n_cols, dtype=np.float32)
        else:
            g_old = np.array(guess, dtype=np.float32)
            g_old[g_old < 1e-20] = 1e-20

        # List of emissivities -----------------------------------------------------
        g_list = []

        # Weight matrix ------------------------------------------------------------
        W = np.diag(1.0 / np.abs(g_old))

        # Fisher information (weighted derivatives) --------------------------------
        DtWDh = np.dot(Dht, np.dot(W, Dh))
        DtWDv = np.dot(Dvt, np.dot(W, Dv))

        # Inversion and calculation of vector g, storage of first guess ------------

        inv = np.linalg.inv(alpha_1 * DtWDh + alpha_2 * DtWDv + PtP + alpha_3 * ItIo + alpha_4 * ItIi)

        M = np.dot(inv, Pt)

        g_old = np.dot(M, f)

        # first_g = np.array(g_old)
        if iterations:
            g_list.append(np.asnumpy(g_old.reshape((n_rows, n_cols))))

        # Iterative process --------------------------------------------------------
        for i in range(2, max_iterations + 1):

            g_old[g_old < 1e-20] = 1e-20

            W = np.diag(1.0 / np.abs(g_old))

            DtWDh = np.dot(Dht, np.dot(W, Dh))
            DtWDv = np.dot(Dvt, np.dot(W, Dv))

            inv = np.linalg.inv(alpha_1 * DtWDh + alpha_2 * DtWDv + PtP + alpha_3 * ItIo + alpha_4 * ItIi)

            M = np.dot(inv, Pt)

            g_new = np.dot(M, f)

            cov = 1. - 0.5 * ((np.corrcoef(g_new, g_old)) +
                              np.corrcoef(g_new.reshape((n_rows, n_cols)).T.flatten(),
                                          g_old.reshape((n_rows, n_cols)).T.flatten()))
            error = cov[0, 1]

            if verbose:
                print("Iteration %d changed by %.4f%%" % (i, error * 100.))

            g_old = np.array(g_new)  # Explicitly copy because python will not

            if iterations:
                g_list.append(np.asnumpy(g_new.reshape((n_rows, n_cols))))

            if ((i >= 5) and (error > 0.90)) or np.isnan(error):
                print("WARNING: Minimum Fisher is not converging, aborting...")
                convergence_flag = False
                break

            elif error < stop_criteria:
                print("Minimum Fisher converged after %d iterations." % i)
                convergence_flag = True
                break

            elif i == max_iterations:  # Break just before the `for loop` does
                print("WARNING: Minimum Fisher did not converge after %d iterations." % i)
                convergence_flag = False
                break

        if not iterations:
            g_list.append(np.asnumpy(g_new.reshape((n_rows, n_cols))))

        return InnerLoopOutput(iterations=g_list, n_rows=n_rows, n_cols=n_cols, convergence_flag=convergence_flag)

    def reconstruction_gpu(self, signals, stop_criteria, alpha_1, alpha_2, alpha_3, alpha_4, max_iterations,
                           iterations=False, guess=None, verbose=False):
        """Apply the minimum fisher reconstruction algorithm for a given set of measurements from tomography.

        input:
            signals: array
                Array of signals from each sensor ordered like in "projections".
            stop_criteria: float
                Average different between iterations to admit convergence as a percentage between 0 and 1.
            alpha_1, alpha_2, alpha_3, alpha_4: float
                Regularization weights. Horizontal derivative. Vertical derivative. Outside Norm. Inside Norm.
            max_iterations: int
                Maximum number of iterations before algorithm admits non-convergence
            iterations: boolean, optional
                If set to `True`, returns every iteration step as a reconstruction. Defaults to False.
            guess: ndarray, optional
                Initial guess for the reconstructed profile.
            verbose: boolean, optional
                Print inner convergence messages. Defaults to `False`

        output:
            inner_loop_output: InnerLoopOutput class instance
                Object holding the data from the mfi inner loop.
        """

        # Aliasing for cleaner code --------------------------------------------------
        # Dh = self._cp.array(self._Dh, dtype=cp.float32)
        # Dht = cp.transpose(Dh)
        # Dv = cp.array(self._Dv, dtype=cp.float32)
        # Dvt = cp.transpose(Dv)
        # Pt = cp.array(self._Pt, dtype=cp.float32)
        n_rows = self._n_rows
        n_cols = self._n_cols

        Dh = self._gpu_Dh
        Dht = self._gpu_Dht
        Dv = self._gpu_Dv
        Dvt = self._gpu_Dvt
        Pt = self._gpu_Pt
        PtP = self._gpu_PtP
        ItIi = self._gpu_ItIi
        ItIo = self._gpu_ItIo

        # Instantiate f vector of signals --------------
        f = cp.array(signals, dtype=cp.float32)

        # -----------------------------  FIRST ITERATION  -------------------------------------------------------------

        # First guess to g is uniform plasma distribution --------------------------
        if guess is None:
            g_old = cp.ones(n_rows * n_cols, dtype=cp.float32)
        else:
            g_old = cp.array(guess, dtype=cp.float32)
            g_old[g_old < 1e-20] = 1e-20

        # List of emissivities -----------------------------------------------------
        g_list = []

        # Weight matrix ------------------------------------------------------------
        W = cp.diag(1.0 / cp.abs(g_old))
        # cp.asnumpy(W)

        # Fisher information (weighted derivatives) --------------------------------
        DtWDh = cp.dot(Dht, cp.dot(W, Dh))
        # cp.asnumpy(DtWDh)
        DtWDv = cp.dot(Dvt, cp.dot(W, Dv))
        # cp.asnumpy(DtWDh)

        # Inversion and calculation of vector g, storage of first guess ------------

        inv = cp.linalg.inv(alpha_1 * DtWDh + alpha_2 * DtWDv + PtP + alpha_3 * ItIo + alpha_4 * ItIi)
        # cp.asnumpy(inv)

        M = cp.dot(inv, Pt)
        # cp.asnumpy(M)

        g_old = cp.dot(M, f)
        # cp.asnumpy(g_old)

        # first_g = cp.array(g_old)
        if iterations:
            g_list.append(cp.asnumpy(g_old.reshape((n_rows, n_cols))))

        # Iterative process --------------------------------------------------------
        for i in range(2, max_iterations + 1):

            g_old[g_old < 1e-20] = 1e-20

            W = cp.diag(1.0 / cp.abs(g_old))
            # cp.asnumpy(W)

            DtWDh = cp.dot(Dht, cp.dot(W, Dh))
            # cp.asnumpy(DtWDh)
            DtWDv = cp.dot(Dvt, cp.dot(W, Dv))
            # cp.asnumpy(DtWDv)

            inv = cp.linalg.inv(alpha_1 * DtWDh + alpha_2 * DtWDv + PtP + alpha_3 * ItIo + alpha_4 * ItIi)
            # cp.asnumpy(inv)

            M = cp.dot(inv, Pt)
            # cp.asnumpy(M)

            g_new = cp.dot(M, f)
            # cp.asnumpy(g_new)

            # plt.figure()
            # plt.imshow(g_new.reshape((n_rows, n_cols)))

            # error = np.sum(np.abs((g_new[g_new > 1e-5] - g_old[g_new > 1e-5]) / g_new[g_new > 1e-5])) / len(g_new > 1e-5)
            # error = cp.sum(cp.abs(g_new - g_old)) / cp.sum(cp.abs(first_g))
            # error = cp.sum(cp.abs(g_new - g_old)**2) / cp.max(g_new)**2
            cov = 1. - 0.5*((cp.corrcoef(g_new, g_old)) +
                              cp.corrcoef(g_new.reshape((n_rows, n_cols)).T.flatten(),
                                          g_old.reshape((n_rows, n_cols)).T.flatten()))
            error = cov[0, 1]
            # cp.asnumpy(error)

            if verbose:
                print("Iteration %d changed by %.4f%%" % (i, error * 100.))

            g_old = cp.array(g_new)  # Explicitly copy because python will not
            # TODO: Implement a placeholder to avoid copying
            # cp.asnumpy(g_old)

            if iterations:
                g_list.append(cp.asnumpy(g_new.reshape((n_rows, n_cols))))

            if ((i >= 5) and (error > 0.90)) or cp.isnan(error):
                print("WARNING: Minimum Fisher is not converging, aborting...")
                convergence_flag = False
                break

            elif error < stop_criteria:
                print("Minimum Fisher converged after %d iterations." % i)
                convergence_flag = True
                break

            elif i == max_iterations:  # Break just before the `for loop` does
                print("WARNING: Minimum Fisher did not converge after %d iterations." % i)
                convergence_flag = False
                break

        if not iterations:
            g_list.append(cp.asnumpy(g_new.reshape((n_rows, n_cols))))

        # return np.array(g_list)
        return InnerLoopOutput(iterations=g_list, n_rows=n_rows, n_cols=n_cols, convergence_flag=convergence_flag)

    def tomogram(self, signals, stop_criteria, alpha_3, alpha_4, target, tolerance, guess, inner_max_iter=10, outer_max_iter=10, verbose=True):
        """Apply the minimum fisher reconstruction algorithm for a given set of measurements from tomography.
        mfi is able to perform multiple reconstructions at a time by employing the rolling iteration.

        input:
            signals: array or list of arrays
                should be an array of single measurements from each sensor ordered like in "projections",
                or a list of such arrays
            stop_criteria: float
                average different between iterations to admit convergence as a percentage.
            comparison: callable
                Comparison function. This function should take one argument which is a 2D array tomogram,
                and output a scalar that reflects the quality of the fit. This function will be minimized with respect
                to the regularization constant.
                TODO: Implement the chi square option and use it as default
            alpha_3, alpha_4: float
                regularization weights: 3 - Outside Norm. 4 - Inside Norm.
            inner_max_iter: int
                Maximum number of iterations of the inner MFI loop
            outer_max_iter: int
                Maximum number of iterations of the outer loop that optimizes the regularization parameter

        output:
            g_list: array or list of arrays
                Reconstructed g vector, or multiple g vectors if multiple signals were provided
            first_g: array
                First iteration g vector. This is the result of the simple Tikhonov regularization
        """

        outer_loop_counter = 0
        chi2 = 0
        lower_limit = None
        upper_limit = None
        reconstruction = 0
        alpha = 0

        def chi_squared(r):
            return np.sum((signals - self.retrofit(r.asarray())) ** 2) / len(signals)

        if self._gpu_enabled:
            def wrapper(a):
                r = self.reconstruction_gpu(signals,
                                            stop_criteria=stop_criteria,
                                            alpha_1=a,
                                            alpha_2=a,
                                            alpha_3=alpha_3,
                                            alpha_4=alpha_4,
                                            max_iterations=inner_max_iter,
                                            verbose=verbose)

                if not r.converged():
                    raise ValueError("Minimum fisher did not converge for alpha=%.2E." % a)
                else:
                    return r
        else:
            def wrapper(a):
                r = self.reconstruction(signals,
                                        stop_criteria=stop_criteria,
                                        alpha_1=a,
                                        alpha_2=a,
                                        alpha_3=alpha_3,
                                        alpha_4=alpha_4,
                                        max_iterations=inner_max_iter,
                                        verbose=verbose)

                if not r.converged():
                    raise ValueError("Minimum fisher did not converge for alpha=%.2E." % a)
                else:
                    return r

        def test_chi2():
            nonlocal outer_loop_counter
            nonlocal chi2
            nonlocal lower_limit
            nonlocal upper_limit
            nonlocal reconstruction
            nonlocal alpha
            chi2 = chi_squared(reconstruction)
            print("Chi2: %lf" % chi2) if verbose else None
            if chi2 > (target + tolerance):
                # Set upper limit compute new guess
                set_up()
            elif chi2 < (target - tolerance):
                # Set lower limit compute new guess
                set_low()
            else:
                # MFI converged beautifully
                raise AssertionError()

        def set_up():
            nonlocal outer_loop_counter
            nonlocal chi2
            nonlocal lower_limit
            nonlocal upper_limit
            nonlocal reconstruction
            nonlocal alpha
            # Guess new alpha
            upper_limit = alpha
            print("Setting upper limit %.2E" % alpha) if verbose else None
            if lower_limit is None:
                alpha = alpha / 10
            else:
                alpha = (alpha + lower_limit) / 2
            print("Next alpha is: %.2E" % alpha) if verbose else None

            try:
                if outer_loop_counter > outer_max_iter:
                    raise StopIteration()
                outer_loop_counter += 1
                reconstruction = wrapper(alpha)
                test_chi2()
            except ValueError:
                if verbose:
                    print("Minimum fisher did not converge for alpha: %.2E" % alpha)
                set_low()

        def set_low():
            nonlocal outer_loop_counter
            nonlocal chi2
            nonlocal lower_limit
            nonlocal upper_limit
            nonlocal reconstruction
            nonlocal alpha
            # Guess new alpha
            lower_limit = alpha
            print("Setting lower limit %.2E" % alpha) if verbose else None
            if upper_limit is None:
                alpha = alpha * 10
            else:
                alpha = (alpha + upper_limit) / 2
            print("Next alpha is: %.2E" % alpha) if verbose else None

            try:
                if outer_loop_counter > outer_max_iter:
                    raise StopIteration()
                outer_loop_counter += 1
                reconstruction = wrapper(alpha)
                test_chi2()
            except ValueError:
                if verbose:
                    print("Minimum fisher did not converge for alpha: %.2E" % alpha)
                set_up()

        # FIRST GUESS ------------------------------------------
        alpha = guess
        if verbose:
            print("Starting MFI with alpha: %.2E" % alpha)
        try:
            outer_loop_counter += 1
            reconstruction = wrapper(alpha)
        except ValueError:
            raise ValueError("Minimum fisher did not converge for the initial guess of alpha: %.2E" % guess)

        try:
            test_chi2()
        except AssertionError:
            print("Minimum Fisher converged")
            return OuterLoopOutput(reconstruction.asarray(), alpha, self._n_rows, self._n_cols, convergence_flag=True)
        except StopIteration:
            print("Minimum Fisher reached the maximum iterations")
            return OuterLoopOutput(reconstruction.asarray(), alpha, self._n_rows, self._n_cols, convergence_flag=False)

