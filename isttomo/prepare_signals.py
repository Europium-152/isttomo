import numpy as np
import matplotlib.pyplot as plt
from .data_fetching import local_fetch
from .archive import get_tomography_setup
from .baseline import baseline


def prepare_plasma_signals(shot, verbose=False):
    """Prepare tomography signals for reconstruction. Works only on actual data starting from shot 44880

    - Load the data from the ISTTOK data base or from the cache folder
    - Apply Signal processing techniques to the loaded data e.g. (detrending, baseline removal, filtering)
    - Return a time array with length T and a data array T x 32.
    The data array can be directly served to the reconstruction classes

    Parameters
    ----------
    shot: int
        Shot number.
    verbose: Bool, optional
        Print accessory information useful for debugging. Defaults to False

    Returns
    -------
    signals_time: 1D array
        Time array for the data of each tomography sensor
    signals_data: N x 32 ND-array
        Data from the sensors. Every row corresponds to a time value in `signals_time`.
        Each column corresponds to one sensor.
    """

    signals_data = []
    channels, projections_dic = get_tomography_setup(shot)

    for uid in channels:
        x, t = local_fetch(shot=shot, uid=uid)
        detrend = baseline(x, deg=1)
        data = x - detrend
        time = np.array(t, dtype=np.float32)

        n = 1
        data = np.cumsum(data, axis=0)
        data = (data[n:] - data[:-n]) / n
        data = data[::n]
        data = np.clip(data, 0., None)
        time = time[n // 2::n]
        time = time[:data.shape[0]]
        signals_data.append(data)

        try:
            if not np.allclose(signals_time, time, 0.0, 1.e-8):
                raise ValueError("tomography signals have different time axis")
        except NameError:
            signals_time = time

    signals_data = np.array(signals_data, dtype=np.float32).T

    print(('signals_data:', signals_data.shape, signals_data.dtype)) if verbose else None
    print(('signals_time:', signals_time.shape, signals_time.dtype)) if verbose else None

    # -------------------------------------------------------------------------

    return signals_time, signals_data,  projections_dic
